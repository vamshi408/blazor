﻿using Blazor2021.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blazor2021.Pages
{
    public class EmployeeTeamViewBase : ComponentBase
    {

        protected override async Task OnInitializedAsync()
        {
            InitializeCountries();
            InitializeJobCategories();
            InitializeEmployees();
            cssClas = null;
            await  Task.Run(GetTeamByEmpID);
            //return base.OnInitializedAsync();
        }

        [Parameter]
        public string EmployeeId { get; set; }
        protected string Cordinates { get; set; }

        public string Button_Text { get; set; } = "Hide Footer";
        public string cssClas { get; private set; }
        public EmployeeTeamModel EmployeeTeam { get; set; } = new EmployeeTeamModel();

        public IEnumerable<Employee> Employees { get; set; }

        private List<Country> Countries { get; set; }

        private List<JobCategory> JobCategories { get; set; }

        private List<int> EmployeeTeamDetails { get; set; }

        private void InitializeJobCategories()
        {
            JobCategories = new List<JobCategory>()
    {
        new JobCategory{JobCategoryId = 1, JobCategoryName = "Pie research"},
        new JobCategory{JobCategoryId = 2, JobCategoryName = "Sales"},
        new JobCategory{JobCategoryId = 3, JobCategoryName = "Management"},
        new JobCategory{JobCategoryId = 4, JobCategoryName = "Store staff"},
        new JobCategory{JobCategoryId = 5, JobCategoryName = "Finance"},
        new JobCategory{JobCategoryId = 6, JobCategoryName = "QA"},
        new JobCategory{JobCategoryId = 7, JobCategoryName = "IT"},
        new JobCategory{JobCategoryId = 8, JobCategoryName = "Cleaning"},
        new JobCategory{JobCategoryId = 9, JobCategoryName = "Bakery"},
        new JobCategory{JobCategoryId = 9, JobCategoryName = "Bakery"}
    };
        }

        private void InitializeCountries()
        {
            Countries = new List<Country>
    {
        new Country {CountryId = 1, Name = "Belgium"},
        new Country {CountryId = 2, Name = "Netherlands"},
        new Country {CountryId = 3, Name = "USA"},
        new Country {CountryId = 4, Name = "Japan"},
        new Country {CountryId = 5, Name = "China"},
        new Country {CountryId = 6, Name = "UK"},
        new Country {CountryId = 7, Name = "France"},
        new Country {CountryId = 8, Name = "Brazil"}
    };
        }

        private void InitializeEmployees()
        {
            var e1 = new Employee
            {
                CountryId = 1,
                MaritalStatus = MaritalStatus.Single,
                BirthDate = new DateTime(1989, 3, 11),
                City = "Brussels",
                Email = "bethany@bethanyspieshop.com",
                EmployeeId = 1,
                FirstName = "Bethany",
                LastName = "Smith",
                Gender = Gender.Female,
                PhoneNumber = "324777888773",
                Smoker = false,
                Street = "Grote Markt 1",
                Zip = "1000",
                JobCategoryId = 1,
                Comment = "Lorem Ipsum",
                ExitDate = null,
                JoinedDate = new DateTime(2015, 3, 1)
            };

            var e2 = new Employee
            {
                CountryId = 2,
                MaritalStatus = MaritalStatus.Married,
                BirthDate = new DateTime(1979, 1, 16),
                City = "Antwerp",
                Email = "gill@bethanyspieshop.com",
                EmployeeId = 2,
                FirstName = "Gill",
                LastName = "Cleeren",
                Gender = Gender.Female,
                PhoneNumber = "33999909923",
                Smoker = false,
                Street = "New Street",
                Zip = "2000",
                JobCategoryId = 1,
                Comment = "Lorem Ipsum",
                ExitDate = null,
                JoinedDate = new DateTime(2017, 12, 24)
            };
            var e3 = new Employee
            {
                CountryId = 3,
                MaritalStatus = MaritalStatus.Married,
                BirthDate = new DateTime(1979, 1, 16),
                City = "Antwerp",
                Email = "bandi@bethanyspieshop.com",
                EmployeeId = 2,
                FirstName = "Vamshi",
                LastName = "Bandi",
                Gender = Gender.Male,
                PhoneNumber = "33999909923",
                Smoker = false,
                Street = "New Street",
                Zip = "2000",
                JobCategoryId = 1,
                Comment = "Lorem Ipsum",
                ExitDate = null,
                JoinedDate = new DateTime(2017, 12, 24)
            };

            var e4 = new Employee
            {
                CountryId = 4,
                MaritalStatus = MaritalStatus.Married,
                BirthDate = new DateTime(1979, 1, 16),
                City = "Antwerp",
                Email = "krishna@bethanyspieshop.com",
                EmployeeId = 2,
                FirstName = "Krishna",
                LastName = "Bandi",
                Gender = Gender.Male,
                PhoneNumber = "33999909923",
                Smoker = false,
                Street = "New Street",
                Zip = "2000",
                JobCategoryId = 1,
                Comment = "Lorem Ipsum",
                ExitDate = null,
                JoinedDate = new DateTime(2017, 12, 24)
            };
            var e5 = new Employee
            {
                CountryId = 5,
                MaritalStatus = MaritalStatus.Married,
                BirthDate = new DateTime(1979, 1, 16),
                City = "Antwerp",
                Email = "rajesh@bethanyspieshop.com",
                EmployeeId = 2,
                FirstName = "Rajesh",
                LastName = "Bandi",
                Gender = Gender.Male,
                PhoneNumber = "33999909923",
                Smoker = false,
                Street = "New Street",
                Zip = "2000",
                JobCategoryId = 1,
                Comment = "Lorem Ipsum",
                ExitDate = null,
                JoinedDate = new DateTime(2017, 12, 24)
            };
            Employees = new List<Employee> { e1, e2,e3,e4,e5 };
        }

        private void GetTeamByEmpID()
        {
            System.Threading.Thread.Sleep(3000);
            //List<int> listids=new List<int> { 1, 2, 3, 4, 5 };
            EmployeeTeamModel employeeTeam = new EmployeeTeamModel();
            employeeTeam.EmployeeId =Convert.ToInt32(EmployeeId);
            employeeTeam.ListTeam = Employees.Select(t => new Employee()
                 {
                     EmployeeId = t.EmployeeId,
                     FirstName = t.FirstName,
                     LastName = t.LastName
                 }).ToList();


            //    Employees.Where(s => Convert.ToInt32(EmployeeId) == 1 ? s.EmployeeId != 1 & s.EmployeeId != 5 :
            //Convert.ToInt32(EmployeeId) == 2 ? s.EmployeeId != 1 & s.EmployeeId != 2 & s.EmployeeId != 3 : s.EmployeeId == 0).ToList().Count()>0?
            //Employees.Where(s => Convert.ToInt32(EmployeeId) == 1 ? s.EmployeeId != 1 & s.EmployeeId != 5 :
            //Convert.ToInt32(EmployeeId) == 2 ? s.EmployeeId != 1 & s.EmployeeId != 2 & s.EmployeeId != 3 : s.EmployeeId == 0).ToList().Select(t => new Employee()
            //{
            //    EmployeeId=t.EmployeeId,
            //    FirstName=t.FirstName,
            //    LastName=t.LastName
            //}).ToList():null ;
            EmployeeTeam = employeeTeam;
        }

        protected void Mouse_Move(MouseEventArgs e)
        {
            Cordinates = $"X={e.ClientX} y={e.ClientY}";
        }

        protected void btnClick()
        {
            if(Button_Text=="Hide Footer")
            {
                Button_Text = "Show Footer";
                cssClas = "HideFooter";
            }
            else
            {
                cssClas = null;
                Button_Text ="Hide Footer";
            }
        }
    }
}
