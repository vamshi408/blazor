﻿using Blazor2021.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blazor2021.Pages
{
    public partial class BlazorOverView
    {

        protected override Task OnInitializedAsync()
        {
            InitializeBlazorModels();
            InitializeVSTemplateModels();
            InitializeDirectivesModels();
            return base.OnInitializedAsync();
        }

        public IEnumerable<BlazorModel> ListBlazorModels { get; set; }
        public IEnumerable<VSTemplateModel> ListVSTemplateModel { get; set; }
        public IEnumerable<DirectiveModel> ListDirectiveModel { get; set; }


        private void InitializeVSTemplateModels()
        {
            ListVSTemplateModel = new List<VSTemplateModel>()
            { new VSTemplateModel { VSTemplates = "Client Side WebAssembly Standalone" },
            new VSTemplateModel { VSTemplates="Client Side WebAssembly ASP.NET Core Hosted" },
            new VSTemplateModel { VSTemplates = "Client Side WebAssembly Standalone" }
            };
        }
        private void InitializeDirectivesModels()
        {
            ListDirectiveModel = new List<DirectiveModel>()
            { new DirectiveModel  { Directives="@Page"},
            new DirectiveModel  {Directives="@Code" },
            new DirectiveModel  {Directives="@Onclick" },
            new DirectiveModel  { Directives="Hosting page  index.html" },
            new DirectiveModel  { Directives="Blazor we can see .razor files i.e. this are components" },
            new DirectiveModel  {Directives="Name of the Component starts with Upppercase" },
            new DirectiveModel  { Directives="Every method should be initialized at OnInitializedAsync method then only data will be available to utilize on UI" },
            new DirectiveModel  {Directives="When application starts razor files will be converted into class files." }
            };
        }

        private void InitializeBlazorModels()
        {
            ListBlazorModels = new List<BlazorModel>()
    {

                new BlazorModel{BlazorType = "Client Side",
                        Description = "Application is running as a WebAssembly app directly in the browser. \n" +
        "The Application is compiled on forhand & compiled code is then downloaded in client & executes their.\n" +
        "If user clicks on bustton it's handled within the locally running Instance of app. Static,imgs & css get downloaded from server seperately"},

                new BlazorModel {BlazorType = "Server Side",
                        Description = "Application will be executed fully on server with ASP.NET Core application.\n" +
               "Between browser & server a SignalR connection is Generated.\n" +
               "If user clicks on bustton it's the request will be sent to the server via signalr & process the request and sents the result back to the client \n" +
               "blazor.server.js"
                }
            };


        }
    }
}
