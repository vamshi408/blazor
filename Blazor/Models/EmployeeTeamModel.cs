﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blazor2021.Models
{
    public class EmployeeTeamModel
    {
        public int EmployeeId { get; set; }
        public List<Employee> ListTeam { get; set; }
    }
}
