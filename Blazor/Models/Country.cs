﻿namespace Blazor2021
{
    public class Country
    {
        public int CountryId { get; set; }
        public string Name { get; set; }
    }
}
