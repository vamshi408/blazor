﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blazor2021.Models
{
    public class BlazorModel
    {
        public string BlazorType { get; set; }
        public string Description { get; set; }
        //public List<VSTemplateModel> listVSTemplates  { get; set;}
        //public List<DirectiveModel> listDirectiveModel { get; set;}
    }

    public class VSTemplateModel
    {
        public string VSTemplates { get; set; }
    }

    public class DirectiveModel
    {
        public string Directives { get; set; }
    }
}
