﻿namespace Blazor2021
{
    public enum Gender
    {
        Male,
        Female,
        Other
    }
}
