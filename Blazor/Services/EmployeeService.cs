﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;

namespace Blazor2021.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly HttpClient httpClient;

        public EmployeeService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }
        public async Task<Employee> GetEmployeeByID(int id)
        {
            var jsonresult = await httpClient.GetStringAsync($"/api/GetEmployeeByID?{id}");
            return JsonConvert.DeserializeObject<Employee>(jsonresult);
        }

        public async Task<IEnumerable<Employee>> GetEmployees()
        {
            var jsonresult= await httpClient.GetStringAsync($"/api/GetEmployeeList");
            return JsonConvert.DeserializeObject<IEnumerable<Employee>>(jsonresult);
        }
    }
}
